# GSaaS - Monitoring Client

A React client application powered by NextJS, React, ChakraUI and TypeScript.
The application should monitor processes that are ongoing in the GSaaS-Environment.

This project is based on the NextJS [example](docs/project-base.md) for nextjs-with-chakra-ui-typescript.
