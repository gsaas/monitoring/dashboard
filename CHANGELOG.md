# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.7...v0.1.8) (2021-07-31)


### CI

* **graph:** fix needsn definition for release ([f400aa0](https://gitlab.com/gsaas/monitoring/dashboard/commit/f400aa033cce61377ac03dac0b0866da6de9a47f))

### [0.1.7](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.6...v0.1.7) (2021-07-31)


### Features

* **components:** add avatar component for menu ([e132e10](https://gitlab.com/gsaas/monitoring/dashboard/commit/e132e1023f58cc918e648adde55574dcdca4adbb)), closes [#20](https://gitlab.com/gsaas/monitoring/dashboard/issues/20)
* **components:** add card container component ([812374e](https://gitlab.com/gsaas/monitoring/dashboard/commit/812374e3b7e05c7ed1c9ec9b8e763eab7c7147a1)), closes [#22](https://gitlab.com/gsaas/monitoring/dashboard/issues/22)
* **components:** add icon component ([5732649](https://gitlab.com/gsaas/monitoring/dashboard/commit/5732649c2e0be903685e6ff413c876de1ac09a90)), closes [#41](https://gitlab.com/gsaas/monitoring/dashboard/issues/41)
* **components:** add link component that wraps next and chakralink ([862fa0a](https://gitlab.com/gsaas/monitoring/dashboard/commit/862fa0a83423eb86981819d7f0bde5aa4f7eb93d))
* **components:** mark footerlink as external ([8464e5c](https://gitlab.com/gsaas/monitoring/dashboard/commit/8464e5cb024e5b7edb8ae46b5693cec6da7e51ef)), closes [#42](https://gitlab.com/gsaas/monitoring/dashboard/issues/42) [#13](https://gitlab.com/gsaas/monitoring/dashboard/issues/13)


### Bug Fixes

* **components:** fix wrong url in side menu item ([e62fd23](https://gitlab.com/gsaas/monitoring/dashboard/commit/e62fd2302543eba6fcdcfdf53dd60ff467cf07d3))
* **default-export:** fix imports in _app ([21abc78](https://gitlab.com/gsaas/monitoring/dashboard/commit/21abc78a17ca54c4da2dfd805c61e9d02c791d56))


### Others

* **eslint:** add stricter eslint-configuration ([c07b85d](https://gitlab.com/gsaas/monitoring/dashboard/commit/c07b85da0386d532f07d2f4f85d1461632787a7d))


### Styling

* **components:** add whitespace to link component children ([4922e3b](https://gitlab.com/gsaas/monitoring/dashboard/commit/4922e3be18e9b22aa78d5aa631dbed6480919bb8))
* **components:** apply requested changes ([422b862](https://gitlab.com/gsaas/monitoring/dashboard/commit/422b86267600ce3177d141e1c2fa8a804b4e11ca))
* **eslint-errors:** fix eslint-errors ([8830ff0](https://gitlab.com/gsaas/monitoring/dashboard/commit/8830ff04f70f027cb4b4371f9191a51fd7f4d4ef))


### CI

* **rules:** adjust changes condition in rule-set ([a142434](https://gitlab.com/gsaas/monitoring/dashboard/commit/a142434099b14701e74114c512aa3fc9f8fcce8e))
* **steps:** update DAG  ([c4f33c6](https://gitlab.com/gsaas/monitoring/dashboard/commit/c4f33c6ec35f9917396badd9b88128ebbd4a878e))


### Code Refactoring

* **components:** add icons to application ([a2c0374](https://gitlab.com/gsaas/monitoring/dashboard/commit/a2c0374b867898ff56c81ca313c02df973c424ea)), closes [#29](https://gitlab.com/gsaas/monitoring/dashboard/issues/29) [#37](https://gitlab.com/gsaas/monitoring/dashboard/issues/37)
* **components:** refactor settings button ([6586a9b](https://gitlab.com/gsaas/monitoring/dashboard/commit/6586a9bf2a345e500cac63955af44a774ec0c565))
* **theme:** update theme ([b5a8699](https://gitlab.com/gsaas/monitoring/dashboard/commit/b5a869931d4b97bb1cdfba080e6bf2b1dbf18724))

### [0.1.6](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.5...v0.1.6) (2021-07-13)


### Features

* **menu-item:** add menu-item component ([3e21801](https://gitlab.com/gsaas/monitoring/dashboard/commit/3e2180156ee8f4d63b83f990b5f758b75063b526))
* **router:** add dynamic routes ([ff9fe00](https://gitlab.com/gsaas/monitoring/dashboard/commit/ff9fe0039dd9cf2024ca32a54cfd802866696c9e))
* **side-menu:** add side-menu component ([3c80d61](https://gitlab.com/gsaas/monitoring/dashboard/commit/3c80d61636515509beda00d06402b9ad6f96fa35)), closes [#16](https://gitlab.com/gsaas/monitoring/dashboard/issues/16)
* **side-menu:** integrate side-menu into existing layout ([2d13233](https://gitlab.com/gsaas/monitoring/dashboard/commit/2d1323393192593b2195ac0787c3c829d9ecf6a4))


### Styling

* **dashboard:** reformat code ([97d8c61](https://gitlab.com/gsaas/monitoring/dashboard/commit/97d8c61d17be33e34117208746d7c3b5e8cdd7db))


### CI

* **.gitlab-ci.yml:** add DOCKER_DRIVER variable to step ([60caa59](https://gitlab.com/gsaas/monitoring/dashboard/commit/60caa5922689cb1443b8a592835f37e52db8e1e7))
* **disable step:** disable code-quality in pipeline ([0ad45c2](https://gitlab.com/gsaas/monitoring/dashboard/commit/0ad45c2247b5d14bc5f1aad1995d28ed30ee624a))
* **step-order:** rearrange pipeline steps and add preconditions ([7774e69](https://gitlab.com/gsaas/monitoring/dashboard/commit/7774e697df19280b002e8f790dd25f1a0bb29c8c))

### [0.1.5](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.4...v0.1.5) (2021-07-11)


### Features

* **dummy-pages:** add dummy pages ([5a57ac4](https://gitlab.com/gsaas/monitoring/dashboard/commit/5a57ac4d129de2241a7357ee2ff94d362ee0965d))
* **internationalization:** add next-i18n to project ([c1ef719](https://gitlab.com/gsaas/monitoring/dashboard/commit/c1ef71920c189382c483a57fac6f105b35e07f58)), closes [#14](https://gitlab.com/gsaas/monitoring/dashboard/issues/14)


### Styling

* **server-page:** remove unused imports ([bddf9e0](https://gitlab.com/gsaas/monitoring/dashboard/commit/bddf9e0f5c58dca7406cd740b7a696953609aef6))

### [0.1.4](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.3...v0.1.4) (2021-07-11)


### Styling

* **dashboard:** reformat code ([c78ae61](https://gitlab.com/gsaas/monitoring/dashboard/commit/c78ae615b1936f1bc1da4a215b9e0a27c5ed7d87))

### [0.1.3](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.2...v0.1.3) (2021-07-11)


### Bug Fixes

* **.gitlab-ci.yml:** fix path for step-rule ([e69a6c2](https://gitlab.com/gsaas/monitoring/dashboard/commit/e69a6c2ca0b82354b8b7ed98ad968156b67617bd))


### Code Refactoring

* **header:** reformat imports ([e054f82](https://gitlab.com/gsaas/monitoring/dashboard/commit/e054f82af1e28d6cf71fb767610b70fd6591030c))


### Styling

* **header:** fix import indentation ([5b414ef](https://gitlab.com/gsaas/monitoring/dashboard/commit/5b414efba0f03af653d9fd1b75d310ec32425ca6))
* **layout:** fix indentation ([6cd0872](https://gitlab.com/gsaas/monitoring/dashboard/commit/6cd08727ea6cae054b162f46d18de644bc5ca47e))

### [0.1.2](https://gitlab.com/gsaas/monitoring/dashboard/compare/v0.1.1...v0.1.2) (2021-07-11)


### Bug Fixes

* **divider:** fix infinite react loop ([67ffd1c](https://gitlab.com/gsaas/monitoring/dashboard/commit/67ffd1c0716b83cb3861a2ff260114b5c4312bc3)), closes [#7](https://gitlab.com/gsaas/monitoring/dashboard/issues/7)

### 0.1.1 (2021-07-11)


### Features

* **component:** add header component ([1ecb985](https://gitlab.com/gsaas/monitoring/dashboard/commit/1ecb9853dbd7ab9fa1ba0fd9cf96d5748d1ba410))
* **component:** add list ([1625fa8](https://gitlab.com/gsaas/monitoring/dashboard/commit/1625fa8da28f78001e4cfb9dbc166007a526d2a1))
* **components:** add storybook and example components ([202c62b](https://gitlab.com/gsaas/monitoring/dashboard/commit/202c62b83640878afa0bf26bbe8b5baf64a49ec5))
* **models:** add basic interfaces ([603354f](https://gitlab.com/gsaas/monitoring/dashboard/commit/603354ff8240af654dffb8247501170d6fa845c9))


### Bug Fixes

* **.gitlab-ci-yml:** add yarn to command ([568e894](https://gitlab.com/gsaas/monitoring/dashboard/commit/568e8942b75a3363609d2d06df71ad9e037355a1))
* **ci-configuration:** fix ci-configuration and DAG ([7d4e9ef](https://gitlab.com/gsaas/monitoring/dashboard/commit/7d4e9ef6ce7cb07bcdd5ec5f7d9bbdcfb6c284e2))


### Others

* **init:** add eslint, lint-staged to project ([69774db](https://gitlab.com/gsaas/monitoring/dashboard/commit/69774db051be60fb6372dd5cf9c450c7e6772f78))
* **init:** initialize project with commitlinting, standard-version and editorconfig ([1a8bc10](https://gitlab.com/gsaas/monitoring/dashboard/commit/1a8bc102d7893ea499085ed8030b2c9f038b5d23))


### Docs

* **README:** update readme.md and project init ([332d797](https://gitlab.com/gsaas/monitoring/dashboard/commit/332d797309c3bdc7029a9a3ed2e7160410016b91))


### Code Refactoring

* **component:** make props optional, fix code formatting ([9b062cb](https://gitlab.com/gsaas/monitoring/dashboard/commit/9b062cbb8039d56a3e7d455a86fc61d5c891d107))
* **dependency:** add 'faker' dependency ([49fdfd0](https://gitlab.com/gsaas/monitoring/dashboard/commit/49fdfd01a77f43bb0b3e5b80e32c8af09a90d8a0))
* **testing:** add test mock data, add test pages ([0a4e10c](https://gitlab.com/gsaas/monitoring/dashboard/commit/0a4e10c96015c84fbeeffd200aee6c82d220577c))


### CI

* **.gitlab-ci:** add gitlab-ci configuration base ([3068adf](https://gitlab.com/gsaas/monitoring/dashboard/commit/3068adf321e3fdd036b810ef9713451523307496))
* **ci:** add docker build and auto-release creation to pipeline ([382deb3](https://gitlab.com/gsaas/monitoring/dashboard/commit/382deb33eb090340a032390b337b8cd7b3bd34a7))
* **ci-stages:** add missing quality stage definition ([a4235f2](https://gitlab.com/gsaas/monitoring/dashboard/commit/a4235f286a5b0c466159a7d62edb885f75572ff1))
* **code quality:** add code quality reports to pipeline ([81ab277](https://gitlab.com/gsaas/monitoring/dashboard/commit/81ab27728a13610098e54467716207b68cbf3105))
* **lint:** add code linting to pipeline ([0e6e9d7](https://gitlab.com/gsaas/monitoring/dashboard/commit/0e6e9d73ad75fb363ba5d08e19596ecaaf6ee355))
* **pipeline-configuration:** restructure pipeline and fix missing definitions ([0d76b54](https://gitlab.com/gsaas/monitoring/dashboard/commit/0d76b54cd104845fb800e2f3bf8f0afe24c86e57))
* **prepare-stage:** add typecheck step ([6009cbf](https://gitlab.com/gsaas/monitoring/dashboard/commit/6009cbfda2a5a120c91d765356ea93dcb865250a))
* **quality-stage:** fix undefined job ([8351678](https://gitlab.com/gsaas/monitoring/dashboard/commit/8351678e6b8a5421a1eb44e0d218671fb3be1b2c))
* **quality-stage:** fix undefined needs ([316c6ac](https://gitlab.com/gsaas/monitoring/dashboard/commit/316c6ac6645646c2d0a0fc812b9bcbf59634636e))
* **quality-stage:** fix undefined needs ([56f070f](https://gitlab.com/gsaas/monitoring/dashboard/commit/56f070fd37ed1d6546ab9302a6537877e7b5d146))
* **stages:** add stage to top-level definition ([299eabe](https://gitlab.com/gsaas/monitoring/dashboard/commit/299eabec50379b77391d554b369bf42ab51c1174))
* **workflow:** add workflow rules to prevent duplicate pipeline creation ([a4c118c](https://gitlab.com/gsaas/monitoring/dashboard/commit/a4c118c44564b715eed3236c68ccee587699ade1))
