import { Flex } from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { useRouter } from "next/dist/client/router"
import React, { FC } from "react"
import { List } from "../components/list"
import { EGame } from "../model/server/enum"
import { IGameServer } from "../model/server/interface"
import { TGameServers } from "../model/server/types"
import gameServers from "../testdata/game-server.json"
const gs = gameServers as unknown as TGameServers
const Server: FC = () => {
	const router = useRouter()
	const {
		t
	} = useTranslation("common")
	return (
		<Flex direction="column">
			<Flex pb="2rem">
				{t("pages.server.intro")}
			</Flex>
			<List
				items={gs}
				getText={
					(g: IGameServer) => `${EGame[g.gameName]} - ${g.ip}`
				}
			/>
			<Flex direction="row">
				Element below server list
			</Flex>
		</Flex>
	)
}
export const getStaticProps = async ({ locale }) => ({
	props: {
		...await serverSideTranslations(locale, ["common"])
	}
})
// eslint-disable-next-line import/no-default-export
export default Server
