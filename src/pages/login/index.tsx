import {
	Box,
	Button,
	Center,
	Flex,
	FormLabel,
	Input,
	Text
} from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import React, { FC, useState } from "react"
import { PasswordInput } from "../../components/input/password"
const Login: FC = () => {
	const [password, setPassword] = useState<string>("")
	const [username, setUsername] = useState<string>("")
	const {
		t
	} = useTranslation()
	const handleChange = (event): void => {
		const {
			name,
			value
		} = event.target
		switch (name) {
			case "password":
				setPassword(value)
				break
			case "username":
				setUsername(value)
				break
			default:
				break
		}
	}
	return (
		<Center>
			<Flex
				direction="column"
				margin="3rem 8rem"
				justify="center"
				maxWidth="75%"
			>
				<Text
					fontSize="1.5rem"
					fontWeight="bold"
					justifySelf="center"
					m="0 auto"
				>
					Login
				</Text>
				<Box mb=".5rem">
					<FormLabel mb="0">
						{t("pages.login.username")}
					</FormLabel>
					<Input
						onChange={handleChange}
						placeholder="Enter username"
						_focus={{
							outline: "none"
						}}
					/>
				</Box>
				<Box>
					<FormLabel mb="0">
						{t("pages.login.password")}
					</FormLabel>
					<PasswordInput
						handleChange={handleChange}
					/>
				</Box>
				<Box
					justify="center"
					margin="1rem auto"
				>
					<Button
						size="sm"
						textAlign="center"
						_focus={{
							outline: "none"
						}}
					>
						Login
					</Button>
				</Box>
			</Flex>
		</Center>
	)
}
export const getStaticProps = async ({ locale }) => ({
	props: {
		...await serverSideTranslations(locale, ["common"])
	}
})
// eslint-disable-next-line import/no-default-export
export default Login
