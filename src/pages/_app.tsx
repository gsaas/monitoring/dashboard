import { ChakraProvider, localStorageManager } from "@chakra-ui/react"
import { appWithTranslation } from "next-i18next"
import { AppProps } from "next/app"
import React from "react"
import nextI18NextConfig from "../../next-i18next.config"
import { LayoutContainer } from "../components/layout"
import { theme } from "../theme"
const GSaaS = ({ Component, pageProps }: AppProps) => {
	return (
		<ChakraProvider
			colorModeManager={localStorageManager}
			resetCSS
			theme={ theme }
		>
			<LayoutContainer>
				<Component {...pageProps} />
			</LayoutContainer>
		</ChakraProvider>
	)
}
// eslint-disable-next-line import/no-default-export
export default appWithTranslation(GSaaS, nextI18NextConfig)
