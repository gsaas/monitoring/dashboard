import {
	Flex,
	Text
} from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { useRouter } from "next/dist/client/router"
import React, { FC } from "react"
const UserDetails: FC = () => {
	const router = useRouter()
	const {
		userId
	} = router.query
	const {
		t
	} = useTranslation("common")
	// Const userId = router.pathname.split("/").reverse()[0] ?? "Fallback userId"
	return (
		<Flex>
			<Text>
				{t("pages.user.single.intro")} {userId}
			</Text>
		</Flex>
	)
}
export const getStaticProps = async ({ locale }) => ({
	props: {
		...await serverSideTranslations(locale, ["common"])
	}
})
export const getStaticPaths = async () => {
	return {
		paths: [
			"/user/[userId]"
		],
		fallback: true
	}
}
// eslint-disable-next-line import/no-default-export
export default UserDetails
