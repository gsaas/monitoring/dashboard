import { Flex } from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { useRouter } from "next/dist/client/router"
import React, { FC } from "react"
const User: FC = () => {
	const router = useRouter()
	const {
		t
	} = useTranslation("common")
	return (
		<Flex>
			{t("pages.user.intro")}
		</Flex>
	)
}
export const getStaticProps = async ({ locale }) => ({
	props: {
		...await serverSideTranslations(locale, ["common"])
	}
})
// eslint-disable-next-line import/no-default-export
export default User
