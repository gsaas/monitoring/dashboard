import { extendTheme, ThemeConfig } from "@chakra-ui/react"
import { createBreakpoints } from "@chakra-ui/theme-tools"
const fonts = { mono: "'Menlo', monospace" }
const breakpoints = createBreakpoints({
	sm: "40em",
	md: "52em",
	lg: "64em",
	xl: "80em"
})
const useSystemColorMode = true
const config: ThemeConfig = {
	initialColorMode: "dark",
	useSystemColorMode: useSystemColorMode
}
export const theme = extendTheme({
	breakpoints,
	colors: {
		black: "#16161D"
	},
	config,
	fonts,
	styles: {
		global: {
			body: {
				minHeight: "100%"
			}
		}
	}
})
