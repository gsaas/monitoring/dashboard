import { IIdentifier } from "../interface";
import { IUser } from "./interface";
export type TUser = IUser & IIdentifier
export type TUsers = TUser[]