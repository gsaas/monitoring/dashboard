import { IIdentifier } from "../interface"
import { IGameServer, IServer } from "./interface"

export type TServer = IServer & IIdentifier
export type TGameServers = IGameServer[]