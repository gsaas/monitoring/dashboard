import { TNumbers } from "../types"
import { EGame } from "./enum"
import { TGameServers } from "./types"
import { TUsers } from "../user/types"
export interface IGameServer extends IServer {
  parentId: string,
  gameName: EGame,
  users: TUsers
}
export interface IServer {
  ip: string,
  ports: TNumbers,
  serverSpecs: IServerSpec
}
export interface IHostServer extends IServer {
  gameServers: TGameServers
}
export interface IServerSpec {
  cpuCount: number,
  ramGb: number,
  diskSpaceGb: number
}