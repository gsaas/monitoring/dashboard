import {
  IGameServer,
  IHostServer,
} from "./interface";
export const hasValidAllocation = ({
  serverSpecs: {
    cpuCount: maxCPU,
    ramGb: maxRAM,
    diskSpaceGb: maxDisk
  },
  gameServers
}: Omit<IHostServer, "ip" | "ports">): boolean => {
  let cpuSum = 0
  let ramSum = 0
  let diskSum = 0
  for (const gameServer of gameServers) {
    const {
      cpuCount,
      diskSpaceGb,
      ramGb
    } = gameServer.serverSpecs
    cpuSum += cpuCount
    ramSum += ramGb
    diskSum += diskSpaceGb
    if (cpuSum > maxCPU || ramSum > maxRAM || diskSum > maxDisk) {
      return false
    }
  }
  return true
}
export const canAllocate = (
  {
    serverSpecs,
    gameServers
  }: Omit<IHostServer, "ip" | "ports">,
  gameServer: IGameServer
) => hasValidAllocation({
  serverSpecs,
  gameServers: [...gameServers, gameServer]
})