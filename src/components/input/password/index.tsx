import {
	Button,
	FlexProps,
	Input,
	InputGroup,
	InputRightElement
} from "@chakra-ui/react"
import React, {
	FC,
	ReactElement,
	useState
} from "react"
import { getApplicationIcon } from "../../icon"
export const PasswordInput: FC<{
	handleChange: (event) => void
} & FlexProps> = ({
	handleChange
}): ReactElement => {
	const [shouldShowPassword, setShowPassword] = useState(false)
	const handleClick = () => setShowPassword(!shouldShowPassword)
	const passwordIconString = shouldShowPassword
		? "visible"
		: "hidden"
	const passwordIcon = getApplicationIcon(passwordIconString)
	const passwordInputType = shouldShowPassword
		? "text"
		: "password"
	return (
		<InputGroup size="md">
			<Input
				pr="4.5rem"
				type={ passwordInputType }
				name="password"
				onChange={handleChange}
				placeholder="Enter password"
				_focus={{
					outline: "none"
				}}
			/>
			<InputRightElement width="max-content">
				<Button
					h="1.75rem"
					variant="ghost"
					onClick={handleClick}
					size="sm"
					_focus={{
						outline: "none"
					}}
					_hover={{
						background: "none"
					}}
					_active={{
						background:"none"
					}}
				>
					{ passwordIcon }
				</Button>
			</InputRightElement>
		</InputGroup>
	)
}
