/* eslint-disable @typescript-eslint/no-unused-vars */
import {
	Icon
} from "@chakra-ui/react"
import React, {
	ReactElement
} from "react"
import {
	FaCog,
	FaExternalLinkAlt,
	FaEye,
	FaEyeSlash,
	FaGamepad,
	FaHome,
	FaLock,
	FaLockOpen,
	FaPaypal,
	FaRegCalendar,
	FaRegChartBar,
	FaRegCopy,
	FaRegEdit,
	FaRegHourglass,
	FaRegTrashAlt,
	FaSave,
	FaServer,
	FaSignInAlt,
	FaSignOutAlt,
	FaUnlockAlt,
	FaUser,
	FaUserShield,
	FaUserCheck,
	FaUserCircle,
	FaWallet,
	FaWrench
} from "react-icons/fa"
export const ApplicationIconMap = {
	calendar: FaRegCalendar,
	chartbar: FaRegChartBar,
	copy: FaRegCopy,
	edit: FaRegEdit,
	externallink: FaExternalLinkAlt,
	games: FaGamepad,
	hidden: FaEyeSlash,
	home: FaHome,
	hourglass: FaRegHourglass,
	locked: FaUnlockAlt,
	login: FaSignInAlt,
	logout: FaSignOutAlt,
	payment: FaWallet,
	paypal: FaPaypal,
	save: FaSave,
	server: FaServer,
	settings: FaCog,
	trash: FaRegTrashAlt,
	unlocked: FaLockOpen,
	user: FaUser,
	userconfirm: FaUserCheck,
	visible: FaEye
}
export const getApplicationIcon = (
	iconName: string
): ReactElement => {
	const lowerCasedMenuItem = iconName.toLowerCase()
	const icon = ApplicationIconMap[lowerCasedMenuItem]
	if (!icon) {
		return undefined
	}
	else {
		return <Icon as={icon} mr=".5rem"></Icon>
	}
}
