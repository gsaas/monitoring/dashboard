import {
	Center,
	Divider,
	Flex,
	FlexProps,
	Text
} from "@chakra-ui/react"
import React, { ReactElement } from "react"
import { getApplicationIcon } from "../icon"
import { ExternalLink } from "../link"
import { IFooterProps } from "./interface"
export const Footer = (
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	props: IFooterProps & FlexProps
): ReactElement => {
	return (
		<Flex
			as="footer"
			width="100%"
			py="0.75rem"
		>
			<Flex
				direction="column"
				justifyContent="space-between"
				w="100%"
			>
				<Divider
					mb="0.5rem"
					orientation="horizontal"
				/>
				<Center >
					<ExternalLink
						url="https://gitlab.com/gsaas"
						my=".5rem"
					>
						<Flex>
							<Text mr=".5rem">
								&copy; 2021 GSaaS
							</Text>
							{ getApplicationIcon("externalLink") }
						</Flex>
					</ExternalLink>
				</Center>
			</Flex>
		</Flex>
	)
}
