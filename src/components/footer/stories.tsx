import { Meta as ComponentMeta, Story } from "@storybook/react"
import { IFooterProps } from "./interface"
import { Footer } from "."
const Meta: ComponentMeta = {
	title: "Components/Footer",
	component: Footer
}
export default Meta
const Template: Story = (args: IFooterProps) =>
	<Footer {...args} />

export const Default = Template.bind({})
Default.args = {
	exampleLabel: "Hello",
	exampleSubLabel: "World"
}
