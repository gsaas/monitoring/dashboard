import {
	Link as ChakraLink
} from "@chakra-ui/react"
import Link from "next/link"
import React, {
	FC,
	ReactElement
} from "react"
import {
	TLinkProps
} from "./type"
export const InternalLink: FC<TLinkProps> = ({
	children,
	url,
	...rest
}): ReactElement => {
	return (
		<Link href={ url } passHref>
			<ChakraLink
				{...rest}
				_hover={{
					textDecoration: "none"
				}}
				_focus={{
					boxShadow: "none"
				}}
				userSelect="none"
			>
				{children}
			</ChakraLink>
		</Link>
	)
}
export const ExternalLink: FC<TLinkProps> = ({
	children,
	url,
	...chakraProps
}): ReactElement => {
	return (
		<Link href={ url } passHref>
			<ChakraLink {...chakraProps} isExternal>
				{children}
			</ChakraLink>
		</Link>
	)
}
