import { LinkProps } from "@chakra-ui/react"
export type TLinkProps = {
	url: string
} & LinkProps
