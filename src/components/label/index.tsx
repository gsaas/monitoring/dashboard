import { Text } from "@chakra-ui/react"
import { ReactElement } from "react"
import { ILabelProps } from "./interface"
export const Label = (
	{
		text
	}: ILabelProps
): ReactElement => {
	return (
		<Text as="span">
			{ text }
		</Text>
	)
}
