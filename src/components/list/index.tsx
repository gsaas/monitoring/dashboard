import { ListItem, OrderedList, UnorderedList } from "@chakra-ui/react"
import { ReactElement } from "react"
interface IListProps<T extends unknown> {
	items: T[],
	getText: (t: T) => string,
	isOrdered?: boolean
}
export const List = (
	{
		items,
		getText,
		isOrdered = false
	}: IListProps<unknown>
): ReactElement => {
	const ListType = isOrdered ? OrderedList : UnorderedList
	return (
		<ListType spacing={3} listStyleType="none">
			{
				items.map((item, idx) =>
					<ListItem key={`list-item-${idx}`}>
						{getText(item)}
					</ListItem>
				)
			}
		</ListType>
	)
}
