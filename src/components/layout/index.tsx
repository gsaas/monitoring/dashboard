import {
	Flex,
	Box,
	Spacer
} from "@chakra-ui/react"
import React, { FC } from "react"
import { Footer } from "../footer"
import { Header } from "../header"
import { SideMenu } from "../side-menu"
// eslint-disable-next-line @typescript-eslint/ban-types
export const LayoutContainer: FC = (
	{ children }
) => {
	return (
		<Flex
			bg="#1F1E1F"
		>
			<SideMenu/>
			<Flex
				direction="column"
				minHeight="100vh"
				w="100%"
			>
				<Header/>
				<Box
					width="95%"
					boxSizing="border-box"
					marginX="auto"
					p="1rem .5rem"
				>
					{ children }
				</Box>
				<Spacer/>
				<Footer/>
			</Flex>
		</Flex>
	)
}
