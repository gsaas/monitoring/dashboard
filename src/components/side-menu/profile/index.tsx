import {
	Avatar,
	Box,
	Flex
} from "@chakra-ui/react"
import React, { FC, ReactElement } from "react"
export const ProfileAvatar: FC = (): ReactElement => {
	return (
		<Flex
			direction="column"
			alignItems="center"
		>
			<Avatar
				m=".5rem"
				name="G. SaaS Admin"
				size="xl"
				src="https://bit.ly/dan-abramov"
			/>
			<Box alignItems="center">
				G.SaaS Admin
			</Box>
		</Flex>
	)
}
