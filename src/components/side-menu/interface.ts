export interface IMenuItemProps {
	label: string,
	url?: string
}
