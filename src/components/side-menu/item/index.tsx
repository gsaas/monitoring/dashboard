import {
	Flex,
	Icon,
	Text
} from "@chakra-ui/react"
import React, { FC, ReactElement } from "react"
import { ApplicationIconMap } from "../../icon"
import { InternalLink } from "../../link"
import { IMenuItemProps } from "../interface"
export const MenuItem: FC<IMenuItemProps> = ({ label, url }) => {
	const getItemIcon = (menuItem: string): ReactElement => {
		const lowerCasedMenuItem = menuItem.toLowerCase()
		const icon = ApplicationIconMap[lowerCasedMenuItem]
		if (!icon) {
			return undefined
		} else {
			return <Icon as={icon} mr=".5rem"/>
		}
	}
	return (
		<Flex
			bg="#641321"
			minHeight="max-content"
			p="0.5rem 1rem"
			m=".25rem"
			borderRadius="5px"
			justifyContent="baseline"
			lineHeight="1rem"
			_hover={{
				bg: "#591D27"
			}}
		>
			<InternalLink
				py="0.2rem"
				url={url}
				width="100%"
				pb="-0.5rem"
			>
				<Text>
					{
						getItemIcon(label)
					}
					{ label }
				</Text>
			</InternalLink>
		</Flex>
	)
}
