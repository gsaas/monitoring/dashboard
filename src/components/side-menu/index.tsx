import {
	Box,
	Flex
} from "@chakra-ui/react"
import React, { FC } from "react"
import { InternalLink } from "../link"
import { MenuItem } from "./item"
import { ProfileAvatar } from "./profile"
export const SideMenu: FC = () => {
	const hasLoggedInUser = true
	const cssDisplayValue = hasLoggedInUser
		? "flex"
		: "none"
	return (
		<Flex
			direction="column"
			display={cssDisplayValue}
			bg="#290007"
			h="100vh"
			padding="1rem 1rem"
			justify="space-between"
			width="20%"
			minWidth="150px"
		>
			<Flex direction="column">
				<Box
					p=".5rem"
					alignContent="center"
					mb="1.5rem"
				>
					<InternalLink
						url={"/user/gsaas-00"}
						_hover={{textDecoration:"none"}}
					>
						<ProfileAvatar/>
					</InternalLink>
				</Box>
				<Box>
					{
						["Home", "Games", "Server", "User"].map(
							(item) => {
								const lowerCasedItem = item.toLowerCase()
								const url = lowerCasedItem === "home"
									? ""
									: lowerCasedItem
								return (
									<MenuItem
										label={item}
										key={`menu-item-${item}`}
										url={`/${url}`}
									/>
								)
							}
						)
					}
				</Box>
			</Flex>
			<InternalLink
				url="/settings"
			>
				<MenuItem
					key="menu-item-settings"
					label="Settings"
					url="/settings"
				/>
			</InternalLink>
		</Flex>
	)
}
