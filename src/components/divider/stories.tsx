import {
	Meta as ComponentMeta, Story
} from "@storybook/react"
import { IDividerProps } from "./interface"
import { Divider } from "."
const Meta: ComponentMeta = {
	title: "Components/Divider",
	component: Divider
}
export default Meta
const Template: Story<IDividerProps> = (
	args: IDividerProps
) => <Divider {...args}/>
export const Default = Template.bind({})
Default.args = {
	orientation: "horizontal"
}
