import {
	Divider as ChakraDivider
} from "@chakra-ui/react"
import { ReactElement } from "react"
import { IDividerProps } from "./interface"
export const Divider = (dividerProps: IDividerProps): ReactElement => {
	const {
		orientation
	} = dividerProps
	return (
		<ChakraDivider orientation={ orientation }/>
	)
}
