import { TDividerOrientation } from "./type"
export interface IDividerProps {
	orientation?: TDividerOrientation
}
