import {
	Box,
	Flex,
	Text,
	Spacer,
	Button
} from "@chakra-ui/react"
import { useTranslation } from "next-i18next"
import React, { FC } from "react"
import { Divider } from "../divider"
import { Label } from "../label"
import { InternalLink } from "../link"
export const Header: FC = () => {
	const {
		t
	} = useTranslation()
	return (
		<Flex direction="column">
			<Flex
				as="header"
				direction="row"
				p="15"
				marginBottom="-0.5rem"
			>
				<Box>
					<InternalLink
						url="/"
					>
						<Label
							text="GSaaS - Game Servers as a Service"
						/>
					</InternalLink>
				</Box>
				<Spacer />
				<InternalLink
					url="/login"
				>
					<Text
						mr={["0", "2"]}
						_hover={{
							cursor: "pointer"
						}}
					>
						<Button
							aria-label="login-button"
							size="sm"
						>
							{t("buttons.login")}
						</Button>
					</Text>
				</InternalLink>
			</Flex>
			<Divider orientation="horizontal"/>
		</Flex>
	)
}
