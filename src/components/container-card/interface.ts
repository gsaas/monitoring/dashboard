import { FlexProps } from "@chakra-ui/react"
export interface IContainerCardProps extends FlexProps {
	isFullWidth?: boolean,
	title?: string
}
