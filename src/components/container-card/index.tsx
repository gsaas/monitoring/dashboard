import {
	Box,
	Flex,
	Heading
} from "@chakra-ui/react"
import React, { FC } from "react"
import { IContainerCardProps } from "./interface"
export const ContainerCard: FC<IContainerCardProps> = ({
	children,
	isFullWidth,
	title,
	width: widthProps
}) => {
	const width: string = isFullWidth
		? "100%"
		: widthProps as string ?? "auto"
	return (
		<Flex
			bg="#641321"
			width={width}
			borderRadius="0.75rem"
			border="2px solid #0000004C"
			boxShadow="-1px 2px 5px #0000004C"
			display="inline-block"
			m="0.5rem"
			p=".75rem 1rem"
		>
			<Flex
				direction="column"
			>
				{
					title && (
						<Heading
							size="sm"
							py=".2rem"
							display="block"
						>
							{ title }
						</Heading>
					)
				}
				<Box>
					{children}
				</Box>
			</Flex>
		</Flex>
	)
}
