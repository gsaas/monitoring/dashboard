const path = require("path")
const toPath = (_path) => path.join(process.cwd(), _path)
const { resolve } = require("path");
module.exports = {
    "stories": [
        "../src/components/**/stories.@(tsx)"
    ],
    "addons": [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        {
            name: '@storybook/addon-docs',
            options: {
                configureJSX: true,
            },
        }
    ],
    "typescript": {
        reactDocgen: false
    },
    refs: {
        "@chakra-ui": {
            disabled: true
        }
    },
    // https://github.com/chakra-ui/chakra-ui/issues/2527#issuecomment-728161743
    webpackFinal: async(config) => {
        return {
            ...config,
            resolve: {
                ...config.resolve,
                alias: {
                    ...config.resolve.alias,
                    "@emotion/core": toPath("node_modules/@emotion/react"),
                    "emotion-theming": toPath("node_modules/@emotion/react"),
                },
                symlinks: true
            },
        }
    }
}