import { ChakraProvider, toCSSObject } from "@chakra-ui/react"
// import theme from '@chakra-ui/theme'
import theme from "../src/theme"
export const decorators = [
	Story => (
		<ChakraProvider theme={theme}>
			<Story parameters={parameters} />
		</ChakraProvider>
	)
]
export const parameters = {
	actions: { argTypesRegex: "^on[A-Z].*" },
	theme: theme,
	layout: "centered",
	controls: {
		matchers: {
			color: /(background|color)$/i,
			date: /Date$/,
		},
	},
}
